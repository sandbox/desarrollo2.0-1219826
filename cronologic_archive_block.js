(function ($) {

  Drupal.behaviors.jquerymenu = {
    attach:function(context) {
      jqm_showit = function() {
        $(this).children('.jqm_link_edit').fadeIn();
      }
      jqm_hideit = function() {
        $(this).children('.jqm_link_edit').fadeOut();
      }
      $('ul.jquerymenu li').hover(jqm_showit, jqm_hideit);
      /* add the following line */
      $('ul.jquerymenu .active').parents('li').removeClass('closed').addClass('open');
      /* end adding */
      $('ul.jquerymenu:not(.jquerymenu-processed)', context).addClass('jquerymenu-processed').each(function(){
        $(this).find("li.parent span.parent").click(function(){
          momma = $(this).parent();
          if ($(momma).hasClass('closed')){
            $(momma).find('ul').slideDown('700');
            $(momma).removeClass('closed').addClass('open');
            $(this).removeClass('closed').addClass('open');
          }
          else{
            $(momma).find('ul').slideUp('700');
            $(momma).removeClass('open').addClass('closed');
            $(this).removeClass('open').addClass('closed');
          }
        });
      });
    }
  }

})(jQuery);
