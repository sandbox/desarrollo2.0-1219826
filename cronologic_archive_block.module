<?php

/**
 * @file
 * Cronologic Archive Block listing post grouped by posting year and month
 */

/**
 * Implements hook_block_info().
 */
function cronologic_archive_block_block_info() {
  $blocks['cronologic_archive_block'] = array(
    'info' => t('Cronologic archive block'),
  );
  return $blocks;
}


/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function cronologic_archive_block_block_view($delta = '') {

  switch ($delta) {
    case 'cronologic_archive_block':

      module_load_include('inc', 'archive', 'archive.pages');
      $block['subject'] = t('Cronologic archive');
      if (user_access('access content')) {

        $query = _archive_query('all', _archive_date('all', 0, 0, 0));
        $query->addField('n', 'created');
        $query->addField('n', 'title');

        $result = $query->execute();
        $expanded = 1;

        $menutree = array();
        foreach ($result as $node) {

          $year = date('Y', $node->created);
          $month = date('F', $node->created);

          if (!isset($menutree[$year])) {
            $menutree[$year] = array('data' => '<span class="parent ' . (($expanded)? 'open' : 'closed') . '"></span>' . l($year, 'archive/all/' . $year));
            $menutree[$year]['class'] = array('parent');
            if ($expanded) {
              $menutree[$year]['class'][] = 'open';
            }
            else {
              $menutree[$year]['class'][] = 'closed';
            }
          }

          if (!isset($menutree[$year]['children'][$month])) {

            $menutree[$year]['children'][$month]['data'] = '<span class="parent ' . (($expanded)? 'open' : 'closed') . '"></span>' . l(
                t($month),
                'archive/all/' . $year . '/' . date('m', $node->created)
            );
            $menutree[$year]['children'][$month]['class'] = array('parent');
            if ($expanded) {
              $menutree[$year]['children'][$month]['class'][] = 'open';
            }
            else {
              $menutree[$year]['children'][$month]['class'][] = 'closed';
            }
          }

          $menutree[$year]['children'][$month]['children'][] = l($node->title, 'node/' . $node->nid);
          $expanded = 0;

        }

        if (!isset($menutree) || count($menutree) == 0) {
          $block['content'] = t('No posts to show');
        }
        else {

          drupal_add_css(drupal_get_path('module', 'cronologic_archive_block') . '/cronologic_archive_block.css');
          drupal_add_js(drupal_get_path('module', 'cronologic_archive_block') . '/cronologic_archive_block.js');

          $block['content'] = theme('item_list', array(
              'items' => $menutree,
              'type' => 'ul',
              'attributes' => array(
                'class' => 'jquerymenu',
              ),
            )
          );
        }
      }
  }
  return $block;
}
